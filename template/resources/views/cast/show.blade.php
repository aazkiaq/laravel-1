@extends('layouts.master')

@section('title')
    Detail Cast {{$cast->nama}}
@endsection

@section('content')
    <h1>{{$cast->nama}}</h1>
    <p>{{$cast->umur}} tahun</p>
    <p>{{$cast->bio}} </p>
@endsection