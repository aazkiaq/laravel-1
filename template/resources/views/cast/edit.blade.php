@extends('layouts.master')

@section('title')
    Edit Cast {{$cast->nama}}
@endsection

@section('content')
<form action="/cast/{{$cast->id}} " method="POST">
    @csrf
    @method('put')
    <div class="form-group">
        <label for="title">nama</label>
        <input type="text" class="form-control" value="{{$cast->nama}}" name="nama" id="title" placeholder="Masukkan nama">
        @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="body">umur</label>
        <input type="number" class="form-control" value="{{$cast->umur}}" name="umur" id="umur" placeholder="Masukkan umur">
        @error('umur')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="body">bio</label>
        <textarea name="bio" class="form-control" cols="30" rows="10" placeholder="Masukkan bio">{{$cast->bio}} </textarea>
        @error('bio')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-success">Update</button>
</form>
@endsection